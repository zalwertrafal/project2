# Projekt 2

Aplikacja zestawiająca połącznia , stworzona w NodeJs (backed) oraz AngularJs(frontend)

  

## Pobranie repozytorium

  

1. Uruchom [git](https://git-scm.com/docs) bash lub terminal(cmd)

2. Przejdz do miejsca gdzie chciałbyś zapisać repozytorium

```bash

git clone https://zalwertrafal@bitbucket.org/zalwertrafal/project2.git

```

  

## Oprogramowanie

  

- [Visual Studio Code](https://code.visualstudio.com/)

-  [NodeJS](https://nodejs.org/en/)

  
  
  

1. Otwórz pobrany wcześniej folder za pomocą Visual Studio Code.Następnie w terminalu wpisz polecenie

``` bash
npm install
```

2. Otwórz kolejny terminal

3. Przejdz do folderu _/front_

4. Wpisz ponownie polecenie `npm install`

  

**Uwaga upewnij się ze _npm_ został dodany do zmiennych środowiskowych**

  
  

## Konfiguracja

  

Znajdz plik "dialer.js". W lini 13 znajdziesz pola "login" i "password". Wpisz tam swoje dane logowania do serwera.

  
  

## Uruchamianie

  

1. We wczesniej otwartym terminalu wpisz polecenie

```bash
node dialer.js
```

  

Otrzymasz komunikat "app listening on port 3000" to znaczy ze serwer wystartowal.

  

2. Uruchom kolejny terminal

3. Przejdz do folderu _/front_

```bash
ansible-playbook playbook.yml -i inventory --connection=local -K
```

4. ```bash ng serve```
5. Nastepnie uruchom swoja przeglądarke w polu na adres wpisz `localhost:4200`

  

## Informacje dodatkowe

  

Serwer aplikacji jest ponownie wstawiony do repozytorium ze wzglegu na pewne zmiany jakie zostały dodane od czasu projektu 1.

  

:exclamation: :exclamation: :exclamation: Serwer nadal mozna pobrać pod adresem https://zalwertrafal@bitbucket.org/zalwertrafal/project1_srodowisko.git
