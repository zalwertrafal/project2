import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Call } from './call'
import { Subject, Observable } from "rxjs"
import * as io from "socket.io-client"



@Injectable({
  providedIn: 'root'
})
export class CallService {
  private socket = io("http://localhost:3000")

  readonly STATUS_ANSWERED = "ANSWERED"
  readonly STATUS_FAILED = "FAILED"
  readonly STATUS_NO_ANSWER = "NO ANSWER"
  readonly STATUS_BUSY = "BUSY"
  readonly STATUS_CONNECTED = "CONNECTED"
  private callId = new Subject<number>()
  private callStatus = new Subject<string>()
  private messages = []

  checkStatus(callId) {
    this.http.get<Call>(this.apiUrl + '/status/' + callId)
      .subscribe(data => {
        this.callStatus.next(data.status);
      });
  }
  getCallStatus(): Observable<string> {
    return this.callStatus.asObservable();
  }
  getCallId(): Observable<number> {
    return this.callId.asObservable()
  }
  getMessages(){
    return this.messages;
  }
  send(message) {
    if(message.message.length > 0)
    {
      this.socket.emit('send', message);
    }
  }


  private apiUrl: string = 'http://localhost:3000'
  constructor(private http: HttpClient) {
    this.socket.on("status", status => {
      this.callStatus.next(status)
    });
    this.socket.on("send", obj => {
      this.messages.push(obj);
    });
  }
  placeCall(number: string) {
    const postData = { number1: '999999999', number2: number }
    this.http.post<Call>(this.apiUrl + '/call', postData).subscribe(data => {
      this.callId.next(data.id)
    });
  }

}
