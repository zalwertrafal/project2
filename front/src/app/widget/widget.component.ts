import { Component, OnInit } from '@angular/core';
import { CallService } from '../call.service'

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css']
})
export class WidgetComponent implements OnInit {
  state: string = "waiting"
  userID: string = Math.random().toString(36).substring(3)
  messages: {}
  message: string = ""
  constructor(private callService: CallService) { }


  ngOnInit() {
    this.getMessages();
  }
  //interval: number
  number: string
  validator = /(^[0-9]{9}$)/
  call() {
    if (this.isValidNumber()) {
      this.callService.placeCall(this.number)
      this.state = "ringing"
      this.callService.getCallId().subscribe(callId => {
        this.checkStatus()
        })

    } else {
      console.info("Numer niepoprawny")
    }
  }

  isInvalidMessage(): Boolean {
    if(this.message.length > 0)
    {
      return true
    }
    return false
  }

  send(){
    if(this.isInvalidMessage())
    {
      let message = {
        message: this.message,
        userId: this.userID
      }
      this.callService.send(message);
      this.getMessages();
    }
    else
    {
      console.info('Bład przy wysyłaniu');
    }
  }

  getMessages()
  {
    this.messages = this.callService.getMessages()
  }

  checkStatus() {
    // this.interval = setInterval(() => {
    //   this.callService.checkStatus(callId)
      let state = this.callService.getCallStatus()
      // if (this.state !== state) {
      //   this.state = state
      // }
      // state === "ANSWERED" || state === "FAILED" ||
      // state === "NO ANSWER" || state === "BUSY"
    //   if (
    //     [
    //       this.callService.STATUS_ANSWERED,
    //       this.callService.STATUS_FAILED,
    //       this.callService.STATUS_NO_ANSWER,
    //       this.callService.STATUS_BUSY,
    //       this.callService.STATUS_CONNECTED,
    //     ].indexOf(this.state) !== - 1
    //   ) {
    //     clearInterval(this.interval)
    //   }
    // }, 500)


    this.callService.getCallStatus().subscribe(state => {
      this.state = state
      })
  }

  isValidNumber(): Boolean {
    return this.validator.test(this.number)
  }



}
